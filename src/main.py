from settings import TOKEN
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, ConversationHandler, CallbackContext
from telegram.ext import CallbackQueryHandler
from functions import start, all_result, my_result, help_me, how_it_work, get_source, get_pvalue, standard_cancel, get_backup, still_not_understand
from module.main_conversation import CHANGE, SOLUTION, newtest, choise, show_sol, cancel
import sys
import warnings

warnings.filterwarnings("ignore", message="If 'per_message=False', 'CallbackQueryHandler' will not be tracked for every message")

updater = Updater(token = TOKEN, use_context=True)
ds = updater.dispatcher

#command
ds.add_handler(CommandHandler('start',start))
ds.add_handler(CommandHandler('lista',all_result))
ds.add_handler(CommandHandler('mialista',my_result))
ds.add_handler(CommandHandler('spiegamiilproblema',help_me))
ds.add_handler(CommandHandler('comefunziona',how_it_work))
ds.add_handler(CommandHandler('sorgente',get_source))
ds.add_handler(CommandHandler('pvalue',get_pvalue))
ds.add_handler(CommandHandler('backup',get_backup))
ds.add_handler(CommandHandler('nonhoancoracapito',still_not_understand))

#message
ds.add_handler(MessageHandler(Filters.regex("Risultati Personali"),my_result))
ds.add_handler(MessageHandler(Filters.regex("Risultati Complessivi"),all_result))
ds.add_handler(MessageHandler(Filters.regex("Spiegami il Problema"),help_me))
ds.add_handler(MessageHandler(Filters.regex("Come Funziona"),how_it_work))
ds.add_handler(MessageHandler(Filters.regex("Codice Sorgente"),get_source))

#conversation
test = ConversationHandler(
    entry_points=[CommandHandler('nuovo',newtest),
                  MessageHandler(Filters.regex('Nuovo Test'),newtest)],
    states={
        CHANGE: [CallbackQueryHandler(choise, pattern="^1$"),
                 CallbackQueryHandler(choise, pattern="^2$"),
                 CallbackQueryHandler(choise, pattern="^3$")],
        SOLUTION: [CallbackQueryHandler(show_sol, pattern="^change$"),
                   CallbackQueryHandler(show_sol, pattern="^stay$")]
    },
    fallbacks=[CommandHandler('annulla',cancel)]
)

ds.add_handler(test)

#cancel to use if you are not using ConversationHandler
ds.add_handler(CommandHandler('annulla',standard_cancel))

updater.start_polling()
sys.stdout.write("I Started Listening\n")
