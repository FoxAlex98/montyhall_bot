from telegram import Update, ReplyKeyboardRemove, ReplyKeyboardMarkup
from telegram.ext import CallbackContext
from utils.phrases import welcome_phrase, standard_cancel_phrase, help_phrase, how_it_work_phrase,source_phrase, still_not_understand_phrase
from utils.keyboard import get_keyboard
from utils.query_executor import get_my_tests, get_all_tests, calculate_pvalue
from settings import ADMIN

def start(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id=chat_id, text = welcome_phrase, reply_markup=ReplyKeyboardMarkup(keyboard=get_keyboard(),resize_keyboard=True))

def standard_cancel(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id=chat_id, text = standard_cancel_phrase, reply_markup=ReplyKeyboardMarkup(keyboard=get_keyboard(),resize_keyboard=True))

def help_me(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id=chat_id, text = help_phrase, reply_markup=ReplyKeyboardMarkup(keyboard=get_keyboard(),resize_keyboard=True))

def how_it_work(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id=chat_id, text = how_it_work_phrase, reply_markup=ReplyKeyboardMarkup(keyboard=get_keyboard(),resize_keyboard=True))

def get_source(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id=chat_id, text = source_phrase, reply_markup=ReplyKeyboardMarkup(keyboard=get_keyboard(),resize_keyboard=True))

def my_result(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id=chat_id, text = get_my_tests(chat_id), reply_markup=ReplyKeyboardMarkup(keyboard=get_keyboard(),resize_keyboard=True))

def all_result(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id=chat_id, text = get_all_tests(), reply_markup=ReplyKeyboardMarkup(keyboard=get_keyboard(),resize_keyboard=True))

def get_pvalue(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id=chat_id, text = calculate_pvalue(), reply_markup=ReplyKeyboardMarkup(keyboard=get_keyboard(),resize_keyboard=True))

def get_backup(update, context):
    chat_id = update.message.chat_id
    if(chat_id == ADMIN):
        context.bot.send_document(chat_id=chat_id, document=open('data/montyhall_DB.db', 'rb'), reply_markup=ReplyKeyboardMarkup(keyboard=get_keyboard(),resize_keyboard=True))

def still_not_understand(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id=chat_id, text = still_not_understand_phrase, reply_markup=ReplyKeyboardMarkup(keyboard=get_keyboard(),resize_keyboard=True))
