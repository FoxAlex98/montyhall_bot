from telegram import InlineKeyboardButton, InlineKeyboardMarkup

def get_keyboard():
    return [['Nuovo Test'],["Risultati Personali","Risultati Complessivi"],['Spiegami il Problema','Come Funziona'],['Codice Sorgente']]

def get_Inline_Keyboard():
    door = [[InlineKeyboardButton("1", callback_data=1),
             InlineKeyboardButton("2", callback_data=2),
             InlineKeyboardButton("3", callback_data=3)]]
    return InlineKeyboardMarkup(door)

def get_choise():
    choise = [[InlineKeyboardButton("Cambio", callback_data="change"),
               InlineKeyboardButton("Sto", callback_data="stay")]]
    return InlineKeyboardMarkup(choise)