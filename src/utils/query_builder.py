def get_count_all_tests():
    return "SELECT COUNT(*) FROM Tests"

def get_count_my_tests(chat_id):
    return get_count_all_tests() + " WHERE " + get_my_tests_query(chat_id)

def add_test_query(chat_id,has_changed,is_right,date):
    return "INSERT INTO Tests(chat_id,has_changed,is_right,date) VALUES("+str(chat_id)+","+str(has_changed)+","+str(is_right)+",'"+str(date)+"')"

def get_results(has_changed, is_right):
    return "SELECT COUNT(*) FROM Tests WHERE has_changed = "+ str(has_changed) +" AND is_right = " + str(is_right)

def get_my_results(chat_id,has_changed, is_right):
    return get_results(has_changed, is_right) + " AND " + get_my_tests_query(chat_id)

def get_my_tests_query(chat_id):
    return "chat_id = " + str(chat_id)