def get_results_phrase(stay_wrong, stay_right, change_wrong, change_right, counter):
    return "rimasti che hanno sbagliato        " + str(stay_wrong) + "" + calculate_percentage(stay_wrong,counter) + \
           "\nrimasti che hanno indovinato     " + str(stay_right) + "" + calculate_percentage(stay_right,counter) + \
           "\ncambiati che hanno sbagliato     " + str(change_wrong) + "" + calculate_percentage(change_wrong,counter) + \
           "\ncambiati che hanno indovinato  " + str(change_right) + "" + calculate_percentage(change_right,counter) + \
           "\n\nTotale test eseguiti " + str(counter)

def get_my_results_phrase(stay_wrong, stay_right, change_wrong, change_right, counter):
    return "questi sono i tuoi risultati\n" + get_results_phrase(stay_wrong, stay_right, change_wrong, change_right, counter)


def get_pvalue_phrase(pvalue):
    return "Il pvalue risultante da tutti i risultati finora è " + str(round(pvalue,5))

def calculate_percentage(part, total):
    percentage = (part*100)/total
    return " (" + str(round(percentage,2)) + "%)"

welcome_phrase = "Benvenuto a MontyHall Bot\n Questo bot serve a dimostrare tramite test in prima persona "\
                 "il famoso problema di Monty Hall. \nE se non sai cos'è il problema di Monty Hall, beh questo è un ottimo modo per conoscerlo"

cancel_phrase = "Test annullato correttamente"

standard_cancel_phrase = "Tranquillo, non stavo facendo nulla :("

help_phrase = "usa il comando /nuovo per iniziare un nuovo test, a quel punto ti verrà chiesto di scegliere " \
              "tra 3 porte diverse.\nDietro una di queste porte è presente una macchina, ovvero il premio che dovrai trovare,"\
              "mentre nelle altre 2 sono presenti delle capre.\nUna volta che avrai scelto la porta dove secondo te sta la macchina"\
              " io aprirò un'altra porta dove è presente una capra.\nA questo punto rimarrai con due porte chiuse e ti farò una seconda domanda.\n"\
              "sceglierai di cambiare porta o rimarrai saldo sulla tua scelta?\n" \
              "Se vuoi sapere come funziona il tutto clicca su /comefunziona"

source_phrase = "Ecco il codice sorgente\n https://gitlab.com/FoxAlex98/montyhall_bot"

how_it_work_phrase = "La probabilità di trovare la macchina dietro a una qualsiasi delle porte è pari a 1 o al 100%, infatti aprendole tutte e 3 si è praticamente certi di trovarla.\n " \
                     "A questo punto scegliendo una delle 3 porte, la probabilità di trovare la macchina proprio dietro a quella porta si riduce fino ad 1/3 o il 33.3%, mentre la probabilità di trovare la macchina dietro una delle altre due porte è di 2/3 (il 66.6%).\n " \
                     "A questo punto il conduttore (ricordare che esso sa benissimo dove si trova la macchina) apre un’altra porta che il concorrente NON ha scelto e che nasconde una capra." \
                     "A questo punto ci si ritrovano davanti due porte, una tra queste è quella scelta all’inizio, mentre l’altra è quella che il conduttore ha lasciato. Il concorrente a questo punto (spesso) pensa erroneamente che la probabilità di trovarla in una delle due sia di ½ per ciascuno (o il 50%). " \
                     "In realtà non è così, in quanto la probabilità di trovare la macchina dietro la porta scelta inizialmente era e RIMANE di 1/3, mentre per quanto riguarda la probabilità di trovare la macchina dietro la porta restante è salita fino a 2/3 \nse non hai ancora capito clicca qui /nonhoancoracapito"

do_more_tests_phrase = "non hai ancora fatto molti test\nti invito a farne altri, così da capire meglio il problema\n\n"

enough_tests_phrase = "hai eseguito un buon numero di test\ngrazie per aver contribuito al progetto\n\n"

zero_tests_phrase = "non hai effettuato nessun test, premi  /nuovo per iniziarne uno\n\n"

still_not_understand_phrase = "se il tutto non fosse ancora chiaro, basta immaginare lo stesso problema " \
                       "ma con più di 3 porte, ad esempio 1000 porte dietro ai quali rimane sempre " \
                        "1 macchina e 999 capre. Il concorrente sceglie una porta (probabilità di aver " \
                        "scelto la porta della macchina 1/1000, probabilità di aver sbagliato 999/1000), " \
                        "il presentatore a questo punto chiude le altre 998 porte lasciando come prima solo " \
                        "due porte, quella scelta dal concorrente e quella lasciata aperta dal presentatore. "\
                        "In questo caso però viene molto più semplice credere che la porta scelta all’inizio "\
                        "NON sia quella giusta e che quindi cambiare porta sia la miglior cosa da fare"