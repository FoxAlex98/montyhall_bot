import sqlite3
import pytz
import math
from utils.query_builder import get_count_all_tests, get_count_my_tests, get_results, get_my_results, add_test_query, get_my_tests_query
from utils.phrases import get_results_phrase, get_pvalue_phrase, get_my_results_phrase, do_more_tests_phrase, zero_tests_phrase, enough_tests_phrase
from datetime import datetime, timedelta
from scipy import stats


def add_test(chat_id,has_changed,is_right):
    date = datetime.now(pytz.timezone('Europe/Rome'))
    try:
        dbconn = sqlite3.connect('data/montyhall_DB.db')
        dbconn.execute(add_test_query(chat_id,has_changed,is_right,date))
        dbconn.commit()
    except Exception as error:
        print(error)
    
def get_all_tests():
    try:
        dbconn = sqlite3.connect('data/montyhall_DB.db')
        counter = dbconn.execute(get_count_all_tests()).fetchall()
        stay_wrong = dbconn.execute(get_results(0,0)).fetchall()
        stay_right = dbconn.execute(get_results(0,1)).fetchall()
        change_wrong = dbconn.execute(get_results(1,0)).fetchall()
        change_right = dbconn.execute(get_results(1,1)).fetchall()
        return get_results_phrase(stay_wrong[0][0], stay_right[0][0], change_wrong[0][0], change_right[0][0],counter[0][0])
    except Exception as error:
        print(error)
        return "ERRORE, IMPOSSIBILE RICEVERE I RISULTATI"

def get_my_tests(chat_id):
    response = ""
    try:
        dbconn = sqlite3.connect('data/montyhall_DB.db')
        counter = dbconn.execute(get_count_my_tests(chat_id)).fetchall()
        response += few_test(counter[0][0])
        stay_wrong = dbconn.execute(get_my_results(chat_id,0,0)).fetchall()
        stay_right = dbconn.execute(get_my_results(chat_id,0,1)).fetchall()
        change_wrong = dbconn.execute(get_my_results(chat_id,1,0)).fetchall()
        change_right = dbconn.execute(get_my_results(chat_id,1,1)).fetchall()
        response += get_results_phrase(stay_wrong[0][0], stay_right[0][0], change_wrong[0][0], change_right[0][0],counter[0][0])
        return response
    except Exception as error:
        print(error)
        return "ERRORE, IMPOSSIBILE RICEVERE I RISULTATI"

def few_test(counter):
    if counter == 0:
        return zero_tests_phrase
    elif counter<10:
        return do_more_tests_phrase
    else:
        return enough_tests_phrase

def percentage(percent, total):
    return (percent * total)/100.0

def expected_val(stay_wrong, stay_right, change_wrong, change_right):
    sum_stay = stay_wrong + stay_right
    sum_change = change_wrong + change_right
    expected_values = [[percentage(66.6,sum_stay),percentage(33.3, sum_stay)], [percentage(33.3, sum_change), percentage(66.6, sum_change)]]
    return expected_values

def calculate_pvalue():
    try:
        dbconn = sqlite3.connect('data/montyhall_DB.db')
        stay_wrong = dbconn.execute(get_results(0,0)).fetchall()
        stay_right = dbconn.execute(get_results(0,1)).fetchall()
        change_wrong = dbconn.execute(get_results(1,0)).fetchall()
        change_right = dbconn.execute(get_results(1,1)).fetchall()
        expected_values = expected_val(float(stay_wrong[0][0]), float(stay_right[0][0]), float(change_wrong[0][0]), float(change_right[0][0]))
        observed_values = [[float(stay_wrong[0][0]), float(stay_right[0][0])], [float(change_wrong[0][0]), float(change_right[0][0])]]
        pv = pvalue(observed_values, expected_values)
        return get_pvalue_phrase(pv)
    except Exception as error:
        print(error)
        return "ERRORE, IMPOSSIBILE CALCOLARE IL P-VALUE"

def pvalue(arr1, arr2):
    sum = 0
    rows = len(arr1)
    cols = len(arr1[0])
    for i in range(rows):
        for j in range(cols):
            tmp = math.pow((arr1[i][j] - arr2[i][j]), 2)
            sum+= (tmp/arr2[i][j])
    return stats.distributions.chi2.sf(sum,1)