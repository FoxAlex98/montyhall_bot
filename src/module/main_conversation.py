from module.doors_handler import reset_door, get_door, get_right_door, close_anotherdoor, change_my_mind, is_right_door
from utils.keyboard import get_Inline_Keyboard, get_keyboard, get_choise
from utils.query_executor import add_test
from utils.image_handler import get_car_door, get_goat_door
from utils.phrases import cancel_phrase
from telegram import Update, ReplyKeyboardRemove, ReplyKeyboardMarkup
from telegram.ext import ConversationHandler

CHANGE,SOLUTION = range(2)

chosen_door = 0
wrong_door = 0

def newtest(update, context):
    chat_id = update.message.chat_id
    reset_door()
    get_door()
    reply_markup = get_Inline_Keyboard()
    context.bot.send_photo(chat_id=chat_id, photo=open('images/threedoors.png','rb'), reply_markup=ReplyKeyboardRemove())#immagine da inviare
    context.bot.send_message(chat_id=chat_id, text="scegli una porta", reply_markup=reply_markup)
    return CHANGE

def choise(update, context):
    query = update.callback_query
    chat_id = query.message.chat_id
    message_id = query.message.message_id
    global chosen_door
    chosen_door = int(format(query.data))
    response = "hai scelto la " + str(chosen_door) + "° porta"
    context.bot.edit_message_text(chat_id=chat_id, message_id=message_id, text=response)
    reply_markup = get_choise()
    global wrong_door
    wrong_door = close_anotherdoor(chosen_door)
    change_phrase = "ho escluso la porta " + str(wrong_door)
    closed_door = get_goat_door(wrong_door)
    context.bot.send_photo(chat_id=chat_id, photo = closed_door)
    context.bot.send_message(chat_id=chat_id, text=change_phrase)
    context.bot.send_message(chat_id=chat_id, text="vuoi cambiare o stai?", reply_markup=reply_markup)
    return SOLUTION

def show_sol(update, context):
    query = update.callback_query
    chat_id = query.message.chat_id
    message_id = query.message.message_id
    has_changed = 0
    response = "hai scelto di "
    choise = format(query.data)
    if choise == "change":
        response += "Cambiare"
        has_changed = 1
        global chosen_door
        global wrong_door
        chosen_door=change_my_mind(chosen_door,wrong_door)
    elif choise == "stay":
        response += "Stare"
    
    context.bot.edit_message_text(chat_id=chat_id, message_id=message_id, text=response)
    
    is_right = is_right_door(chosen_door)
    
    if(is_right):
        result = "Bravo, hai indovinato"
    else:
        result = "Sbagliato, la porta giusta era la " + str(get_right_door())
    #TODO: controllo da fare sull'esito della query
    add_test(chat_id,has_changed,is_right)
    car_door = get_car_door(get_right_door())
    context.bot.send_photo(chat_id=chat_id, photo = car_door)
    context.bot.send_message(chat_id=chat_id, text=result, reply_markup=ReplyKeyboardMarkup(get_keyboard()))
    return ConversationHandler.END

def cancel(update, context):
    chat_id = update.message.chat_id
    context.bot.send_message(chat_id=chat_id, text=cancel_phrase, reply_markup=ReplyKeyboardMarkup(get_keyboard()))
    return ConversationHandler.END