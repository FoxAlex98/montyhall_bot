import random

doors = dict()

def reset_door():
    for x in[1,2,3]:
        doors[x] = False

def get_door():
    x = random.randint(1,3)
    doors[x] = True

def is_right_door(user_choise):
    if doors[user_choise] == True:
        return 1
    else:
        return 0
        
def change_my_mind(user_choise,door_closed):
    for x in[1,2,3]:
        if x != user_choise and x!=door_closed:
            return x

def get_right_door():
    for x in[1,2,3]:
        if doors[x] == True:
            return x

def close_anotherdoor(user_choise):
    if doors[user_choise] == True:
        x = random.randint(1,3)
        while x == user_choise:
            x = random.randint(1,3)
        return x
    else:
        for x in[1,2,3]:
            if x != user_choise and doors[x] == False:
                return x