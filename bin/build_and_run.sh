#!/bin/bash

docker stop montyhall_bot

docker container rm montyhall_bot

docker build --rm -t bot:montyhall_bot .

docker run -d --name montyhall_bot bot:montyhall_bot 
