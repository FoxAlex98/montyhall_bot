FROM python:3-slim

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir --disable-pip-version-check --no-python-version-warning -r requirements.txt

COPY src /src
WORKDIR /src

CMD [ "python", "main.py" ]
